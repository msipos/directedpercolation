package dirpolymer;

public class Dirpolymer {
  private static final int NX = 300, NT = 300;
  private static final float p = 0.7055f;
  private static final int power = 10;
  private static final float beta = 1f / 0.001f;

  private static void drawPhi(final Picture pic, int x, int t, final float bw) {
    pic.drawBW(x, t, bw); pic.drawBW(x+1, t, bw);
    pic.drawBW(x, t+1, bw); pic.drawBW(x+1, t+1, bw);
  }

  private static void drawPath(final Picture pic, int x, int t, float f) {
    pic.drawBAlpha(x, t, f); pic.drawBAlpha(x+1, t, f);
    pic.drawBAlpha(x, t+1, f); pic.drawBAlpha(x+1, t+1, f);
  }

  private static void drawPathR(final Picture pic, int x, int t, float f) {
    pic.drawRAlpha(x, t, f); pic.drawRAlpha(x+1, t, f);
    pic.drawRAlpha(x, t+1, f); pic.drawRAlpha(x+1, t+1, f);
  }

  public static void main(String[] args) {
    //final PolField pf = new PolField(NX, NT);
    final PolField pf = new PolField(NX, NT, 10101010);
    pf.initPhi(p);
    pf.initPhiClean(5);
    pf.calcEmin();
    pf.calcEpathFull();

    //final int min = pf.getEminMin(NT-1);

    final Picture pic = new Picture(2*NX, 2*NT);
    pic.paint(255, 255, 255);
    //waterfall(pf, pic);
    all(pf, pic);
    //close(pf, pic);
    pic.writePng("/home/max/output.png");
  }


  private static void waterfall(final PolField pf, final Picture pic) {
    final int min = 0;
    final int max = pf.getEminMax(NT-1);
    final float diff = max - min;
    for (int t = 0; t < NT; t++) {
      final int dx = (t % 2 == 1) ? 1 : 0;

      for (int x = 0; x < NX-1; x++) {
        if (pf.getEmin(x, t) >= 10000) continue;

        if (pf.getPhi(x, t) == 1) {
          drawPhi(pic, dx+2*x, 2*t, 0.3f);
        } else {
          drawPhi(pic, dx+2*x, 2*t, 1.0f);
        }

        if (pf.getEpath(x, t) > -1) {
          float f = (pf.getEpath(x, t) - min) / diff;
          if (power > 0) {
            f = (float) Math.exp(-beta*f);
          } else {
            f = 1f - f;
          }

          drawPath(pic, dx+2*x, 2*t, f-0.25f);
        }
      }
    }
  }


  private static void close(final PolField pf, final Picture pic) {
    final int min = pf.getEminMin(NT-1);
    final int max = pf.getEminMax(NT-1);
    final float diff = max - min;
    for (int t = 0; t < NT; t++) {
      final int dx = (t % 2 == 1) ? 1 : 0;

      for (int x = 0; x < NX-1; x++) {
        if (pf.getEmin(x, t) >= 10000) continue;

        if (pf.getPhi(x, t) == 1) {
          drawPhi(pic, dx+2*x, 2*t, 0.3f);
        } else {
          drawPhi(pic, dx+2*x, 2*t, 1.0f);
        }

        if (pf.getEpath(x, t) > -1) {
          float f = (pf.getEpath(x, t) - min) / diff;
          if (power > 0) {
            f = (float) ((Math.exp(-power*f) - 1.0) / (Math.exp(power) - 1));
          } else {
            f = 1f - f;
          }

          drawPath(pic, dx+2*x, 2*t, f-0.25f);
        }
      }
    }
  }

  private static void all(final PolField pf, final Picture pic) {
    final int min = 0;
    final int max = pf.getEminMax(NT-1);
    final float diff = max - min;
    for (int t = 0; t < NT; t++) {
      final int dx = (t % 2 == 1) ? 1 : 0;

      for (int x = 0; x < NX-1; x++) {
        if (pf.getEmin(x, t) >= 10000) continue;

        if (pf.getPhi(x, t) == 1) {
          drawPhi(pic, dx+2*x, 2*t, 0.3f);
        } else {
          drawPhi(pic, dx+2*x, 2*t, 1.0f);
        }

        if (pf.getEpath(x, t) > -1) {
          drawPathR(pic, dx+2*x, 2*t, 1f);
        }
      }
    }
  }

}
