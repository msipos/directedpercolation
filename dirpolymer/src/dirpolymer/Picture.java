package dirpolymer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Picture {
  private final BufferedImage bi;


  private static final int maskR = (0xFF << 16),
                           maskG = (0XFF << 8),
                           maskB = (0xFF),
                           maskRB = maskR | maskB,
                           maskRG = maskR | maskG,
                           maskGB = maskG | maskB;
  private final int nx, ny;


  public Picture(int nx, int ny) {
    bi = new BufferedImage(nx, ny, BufferedImage.TYPE_INT_RGB);
    this.nx = nx; this.ny = ny;
  }


  // Utilities for draw functions:
  /** Return a full-alpha ARGB value. */
  private static int argb(int red, int green, int blue) {
    return ((red & 0xFF) << 16) | ((green & 0xFF) << 8) | blue;
  }
  private static int norm(float f) {
    if (f < 0.0f) f = 0.0f;
    if (f > 1.0f) f = 1.0f;
    return (int) (f * 255);
  }


  public void drawRAlpha(int x, int y, float f) {
    if (f > 1f) f = 1f;
    if (f < 0f) f = 0f;
    final int prev = bi.getRGB(x, y);
    final int prevR = ((int) ((prev & maskR) * (1f-f))) >> 16;
    final int prevG = ((int) ((prev & maskG) * (1f-f))) >> 8;
    final int prevB = (int) ((prev & maskB) * (1f-f));
    int newR = (int) (f*255) + prevR;
    //int newR = (int) (f*255);
    if (newR > 255) newR = 255;
    drawRGB(x, y, newR, prevG, prevB);
  }

  public void drawBAlpha(int x, int y, float f) {
    if (f > 1f) f = 1f;
    if (f < 0f) f = 0f;
    final int prev = bi.getRGB(x, y);
    final int prevR = ((int) ((prev & maskR) * (1f-f))) >> 16;
    final int prevG = ((int) ((prev & maskG) * (1f-f))) >> 8;
    final int prevB = (int) ((prev & maskB) * (1f-f));
    int newB = (int) (f*255) + prevB;
    if (newB > 255) newB = 255;
    drawRGB(x, y, prevR, prevG, newB);
  }


  public void drawRGB(int x, int y, int r, int g, int b) {
    bi.setRGB(x, y, argb(r, g, b));
  }
  public void drawBW(int x, int y, float f) {
    int c = norm(f);
    bi.setRGB(x, y, argb(c, c, c));
  }
  public void drawGreen(int x, int y, int g) {
    final int prev = bi.getRGB(x, y);
    bi.setRGB(x, y, prev & maskRB | argb(0, g, 0));
  }
  public void drawGreenF(int x, int y, float f) {
    drawGreen(x, y, norm(f));
  }
  public void drawBlue(int x, int y, int b) {
    final int prev = bi.getRGB(x, y);
    final int prevB = prev & maskB;
    if (b > prevB) bi.setRGB(x, y, prev & maskRG | argb(0, 0, b));
  }
  public void drawBlueF(int x, int y, float f) {
    drawBlue(x, y, norm(f));
  }


  public void paint(int r, int g, int b) {
    for (int x = 0; x < nx; x++) {
      for (int y = 0; y < ny; y++) {
        drawRGB(x, y, r, g, b);
      }
    }
  }


  public void writePng(String filename) {
    try {
      ImageIO.write(bi, "png", new File(filename));
    } catch (IOException ex) {
      System.err.println("IO Error: " + ex.getLocalizedMessage());
    }
  }
}
