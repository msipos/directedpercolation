package dirpolymer;

import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;
public class PolField {
  public final int NX, NT;
  public final int[][] Emin;
  public final byte[][] phi;
  public final int[][] Epath; // Epath is -1 if not reachable
  public final RandomGenerator rg;

  private static final int UNREACHABLE = 100000;

  public PolField(int NX, int NT, int seed) {
    this.NX = NX;
    this.NT = NT;

    Emin = new int[NT][NX];
    phi = new byte[NT][NX];
    Epath = new int[NT][NX];

    rg = new Well44497b(seed);
  }

  public PolField(int NX, int NT) {
    this.NX = NX;
    this.NT = NT;

    Emin = new int[NT][NX];
    phi = new byte[NT][NX];
    Epath = new int[NT][NX];

    rg = new Well44497b();
  }

  public void initPhi(final float p) {
    for (int t = 0; t < NT; t++) {
      final byte[] phis = phi[t];
      for (int x = 0; x < NX; x++) {
        phis[x] = (rg.nextFloat() < p) ? ((byte) 0) : ((byte) 1);
      }
    }
  }


  public void initPhiClean(int mt) {
    for (int t = 0; t < mt; t++) {
      final byte[] phis = phi[t];
      for (int x = 0; x < NX; x++) {
        phis[x] = 0;
      }
    }
  }


  public int getPhi(int x, int t) {
    return phi[t][x];
  }


  public int getEpath(int x, int t) {
    return Epath[t][x];
  }


  //   0     1     2     3       t=0
  //      0     1     2     3    t=1
  //   0     1     2     3       t=2
  //      0     1     2     3    t=3
  public void calcEmin() {
    // Initialize Emin of first row.
    {
      final int[] Emins = Emin[0];
      for (int x = 0; x < NX; x++) {
        Emins[x] = UNREACHABLE;
      }
      Emins[NX/2] = 0;
    }

    // Run Emin simulation
    for (int t = 1; t < NT; t++) {
      final int[] prev = Emin[t-1];
      final int[] cur = Emin[t];
      final byte[] phis = phi[t];

      if ((t % 2) == 1) {
        // Propagate to the right
        for (int x = 0; x < NX - 1; x++) {
          cur[x] = Math.min(prev[x], prev[x+1]) + phis[x];
        }
        cur[NX-1] = prev[NX-1] + phis[NX-1];
      } else {
        // Propagate to the left
        cur[0] = prev[0] + phis[0];
        for (int x = 1; x < NX; x++) {
          cur[x] = Math.min(prev[x-1], prev[x]) + phis[x];
        }
      }
    }
  }


  public int getEmin(int x, int t) {
    return Emin[t][x];
  }


  public int getEminMax(int t) {
    int max = Emin[t][0];
    for (int x = 1; x < NX; x++) {
      if (Emin[t][x] > max) { max = Emin[t][x]; }
    }
    return max;
  }


  public int getEminMin(int t) {
    int min = Emin[t][0];
    for (int x = 1; x < NX; x++) {
      if (Emin[t][x] < min) { min = Emin[t][x]; }
    }
    return min;
  }


  /** This calculates Epath from the assumption that all sites at last T are reachable. */
  public void calcEpathFull() {
    // Initialize last step:
    {
      final int t = NT-1;
      for (int x = 0; x < NX-1; x++) {
        Epath[t][x] = Emin[t][x];
      }
    }
    for (int t = NT-2; t >= 0; t--) {
      Epath[t][0] = -1;
      Epath[t][NX-1] = -1;
      for (int x = 1; x < NX-1; x++) {
        // Start by default with unreachable
        Epath[t][x] = -1;

        final int EminThis = Emin[t][x];
        int EminL, EminR;
        int EpathL, EpathR;

        if ((t % 2) == 1) {
          // Propagate to the right
          EminL = Emin[t+1][x] - phi[t+1][x];
          EpathL = Epath[t+1][x];
          EminR = Emin[t+1][x+1] - phi[t+1][x+1];
          EpathR = Epath[t+1][x+1];
        } else {
          // Propagate to the left
          EminL = Emin[t+1][x-1] - phi[t+1][x-1];
          EpathL = Epath[t+1][x-1];
          EminR = Emin[t+1][x] - phi[t+1][x];
          EpathR = Epath[t+1][x];
        }

        // There is an endpoint at the left.
        if (EpathL >= 0) {
          if (EminThis == EminL) Epath[t][x] = EpathL;
        }
        if (EpathR >= 0) {
          // TODO: Compare EpathL and EpathR.
          if (EminThis == EminR) {
            if (Epath[t][x] != -1) {
              if (Epath[t][x] > EpathR) Epath[t][x] = EpathR;
            } else {
              Epath[t][x] = EpathR;
            }
          }
        }
      }
    }
  }
}
