package jdp;

public class AsciiDp {
  public static void display(boolean[] arr){
    for (int i = 0; i < arr.length; i++){
      if (arr[i]) System.out.print("W");
      else System.out.print(".");
    }
    System.out.println();
  }

  public static void main(String[] args){
    if (args.length != 3){
      System.err.println("usage:");
      System.err.println("  java -jar AsciiDp.jar SIZEX SIZEY P");
      System.exit(1);
    }
    int size = Integer.parseInt(args[0]);
    int sizey = Integer.parseInt(args[1]);
    double p = Double.parseDouble(args[2]);
    Dp1 dp = new Dp1(size, Dp1.BOND_STRATEGY);
    dp.setP(p);
    dp.setStateFull();
    for (int i = 0; i < sizey; i++){
      display(dp.getState());
      dp.step();
    }
  }
}
