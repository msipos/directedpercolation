package jdp;

import java.lang.Math;

class Util {
  protected static boolean roll(double p){
    return Math.random() < p;
  }
}

interface Dp1Strategy {
  /**
   * Produce the state of the system in next, given the current
   * state of the system in prev.
   *
   * @param next This is guaranteed to be cleared (all false)
   * @param step This is a boolean whose values oscilates from
   * step to step.
   **/
  public void step(boolean[] prev, boolean[] next, Dp1 dp);
}

class BondStrategy implements Dp1Strategy {
  public void step(boolean prev[], boolean next[], Dp1 dp){
    assert prev.length == next.length;
    final int l = prev.length;
    final double p = dp.p;

    // +0 step
    for (int i = 0; i < l; i++)
      if (prev[i])
        next[i] = Util.roll(p);

    if (dp.cur_step){ // +1 step
      for (int i = 0; i < l-1; i++)
        if (prev[i] && !next[i+1])
          next[i+1] = Util.roll(p);

      // Special case:
      if (prev[l-1] && !next[0])
        next[0] = Util.roll(p);
    } else { // -1 step
      // Special case:
      if (prev[0] && !next[l-1])
        next[l-1] = Util.roll(p);

      for (int i = 1; i < l; i++)
        if (prev[i] && !next[i-1])
          next[i-1] = Util.roll(p);
    }
  }
}

/**
 * Simulation of 1+1-dimensional directed percolation.
 **/
public class Dp1 {
  /** Current state of the system. **/
  private boolean[] cur;
  public boolean[] getState() { return cur; }
  public void setState(boolean[] state) { cur = state; }

  public void setStateFull(){
    for (int i = 0; i < cur.length; i++)
      cur[i] = true;
  }

  public void setStatePortion(double portion){
    int l = cur.length;
    int dl = (int) (((double)l)*portion);
    for (int i = l/2-dl/2; i < l/2+dl/2; i++){
      cur[i] = true;
    }
  }

  public void setStateSingle(){
    for (int i = 0; i < cur.length; i++)
      cur[i] = false;
    cur[cur.length/2] = true;
  }

  /** Temporary array, used for purpose of single-stepping. **/
  private boolean[] tmp;

  public boolean[] getPreviousState() { return tmp; }

  /** Strategy used to simulate. **/
  private Dp1Strategy strategy;

  /** Is current time step odd or even? **/
  protected boolean cur_step;

  /** Bond strategy for simulating 1+1 DP. **/
  public static final Dp1Strategy BOND_STRATEGY = new BondStrategy();

  /** Percolation probability. **/
  protected double p;
  public double getP() { return p; }
  public void setP(double p) { this.p = p; }

  public Dp1(int size, Dp1Strategy strategy) {
    cur = new boolean[size];
    tmp = new boolean[size];
    this.strategy = strategy;
    cur_step = false;
  }

  /** Run one step of the simulation. **/
  public void step(){
    // Clear out tmp
    for (int i = 0; i < tmp.length; i++){
      tmp[i] = false;
    }

    // Run the step
    strategy.step(cur, tmp, this);

    // Oscillate cur_step
    cur_step = !cur_step;

    // Swap cur and tmp
    boolean[] swp = cur;
    cur = tmp;
    tmp = swp;
  }
}
