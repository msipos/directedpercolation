package jdp;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;

public class PngDp {
  public static void main(String[] args){
    if (args.length != 3){
      System.err.println("usage:");
      System.err.println("  java -jar jdp.jar SIZEX SIZEY P");
      System.exit(1);
    }

    int sizex = Integer.parseInt(args[0]);
    int sizey = Integer.parseInt(args[1]);
    double p = Double.parseDouble(args[2]);
    Dp1 dp = new Dp1(sizex, Dp1.BOND_STRATEGY);
    dp.setP(p);
    dp.setStateSingle();

    int black = 255 << 24;
    int white = (255 << 24) + (255 << 16) + (255 << 8) + 255;
    int green = (255 << 24) + (20 << 16) + (150 << 8) + 20;

    BufferedImage bi = new BufferedImage(sizex, sizey, BufferedImage.TYPE_4BYTE_ABGR);
    for (int i = 0; i < sizey; i++){
      boolean state[] = dp.getState();
      dp.step();
      for (int j = 0; j < sizex; j++){
        if (state[j])
          bi.setRGB(j, i, black);
        else
          bi.setRGB(j, i, white);
      }
    }

    try {
      ImageIO.write(bi, "png", new File("dp_picture.png"));
    } catch (IOException e){
      System.err.println("failed to write to 'dp_picture.png':" + e.getMessage());
    }
  }


  private static void drawBorder(int sizey, BufferedImage bi, int black, int sizex) {
    // Draw border
    for (int i = 0; i < sizey; i++){
      bi.setRGB(0, i, black);
      bi.setRGB(sizex-1, i, black);
    }
    for (int i = 0; i < sizex; i++){
      bi.setRGB(i, 0, black);
      bi.setRGB(i, sizey-1, black);
    }
  }
}
